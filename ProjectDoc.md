# 项目文档
## 项目基本框架集成
#### 发布订阅(广播)
- 文档：[EventBus-广播发送与接收.md](http://note.youdao.com/noteshare?id=911c036a4bea47359cacdd9ad8297b9c&sub=A11EBED523104B76B6B18D47CF18092B)
#### 控件视图绑定
- 文档：[ButterKnife - 注解视图绑定.md](http://note.youdao.com/noteshare?id=314d36def9bc3209b5aa4299d25d2bfe&sub=10D70F31DC744FB4BF0F10CF374F4884)
#### 网络请求
- 文档：[Retrofit2+rxjava-网络请求.md](http://note.youdao.com/noteshare?id=d3642922b5445a47f3c057ee3853e95f&sub=0DED09A763254A1E890707EFF4F9C6B1)
- 文档：[Rxjava+RxAndroid.md](http://note.youdao.com/noteshare?id=948090e1de0442cfe79be49f6fc69c04&sub=18DD8ADD7699479E836F4A75B9E2E016)
#### 图片加载
- 文档：[Glide-图片加载.md](http://note.youdao.com/noteshare?id=030ab02e1cc9099b272a257ec7e31275&sub=7A68DDA5AABD4495A05D282CBEC7C2D9)
- [图片渲染](https://github.com/wasabeef/glide-transformations)的文档也在一起
#### 图片压缩
- 文档：[Luban-图片压缩.md](http://note.youdao.com/noteshare?id=2a90b5952b00eb49204b1b77e8a69dec&sub=D938807524DA4E00BB158E4B8C06AFDA)
#### Gson解析
- 文档：[gson-Gson解析.note](http://note.youdao.com/noteshare?id=851cffd5fd729d4353c0140e9e6135ff&sub=A8BCF9DD656F412AB263DBCED55BEE03)
#### 日志打印
- 文档：[logger-日志打印.note](http://note.youdao.com/noteshare?id=12eb464496038804762e513dc6539e89&sub=C788788D711A49DBAB7AF3C511101950)
#### 上拉加载下拉刷新控件
- 文档：[SmartRefreshLayout——Android智能下拉刷新上拉加载框架](http://note.youdao.com/noteshare?id=ad8decf832dbd6d002a6386309cd90e4&sub=1470B52BECEF44B082A2F7A999CABF24)
#### 弹窗集合框架
- 文档：[XPopup —— 各种弹窗集合.md](http://note.youdao.com/noteshare?id=52c918b30b84238dac8b0aa6b1655ea3&sub=EEB8447DCE1F4875B892E329056474EF)
#### 权限申请
- 文档：[AndPermission —— 权限申请.md](http://note.youdao.com/noteshare?id=2af5c5f0eb7c82ff70fd948b25072ca9&sub=56DBAB9C20D04E1F9BF3004387A17283)
#### 快速UI开发依赖
- 文档：[QMUI Android -腾讯开源的Android UI](http://note.youdao.com/noteshare?id=6c8897a5242b498356b661364721f685&sub=E74778E042BC4DD480AEC94654C0D4D6)
## 项目第三方接入
#### 友盟
- 基本包接入版本 2.0.0
##### 友盟统计
- 接入版本8.0.0
- 封装工具类 com.xmqian.app.common.utils.umeng.UMengAnalyticsUtils


## 项目基本模块集成
- 已集成界面，登录/注册/忘记密码(com.xmqian.app.project.ui.user),过渡界面(SplashActivity),引导界面(GuideActivity)
- 已配置好用户信息管理类**com.xmqian.app.common.config.user.UserManager**,配置好**Toast**
- 数据统计：友盟统计8.0，已集成自动页面统计Activity,手动Fragment统计，传入TAG(类文件名),可自行更改，工具类（com.xmqian.app.common.utils.umeng.UMengAnalyticsUtils）