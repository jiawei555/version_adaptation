## 项目资料
##### 项目中使用的自定义的工具类库,如需查看正在使用的可以看[这个](https://github.com/isyue/UtilsLibrary),
开发版本和工具类API文档在[这里](https://gitee.com/xmqian/UtilsLibrary)
## 分支说明
- branch v_1.0;   实现了6.0-9.0的基础适配，可看适配文档,后续新API会更新此分支
- branch v_1.1;   实现深层定制，结合自定义工具类进行封装，不增加第三方依赖
- branch v_2.0;   2.0系列分支，增加必要依赖库,工具检查和第三方服务
- branch v_3.0;    3.0系列分支，增加项目中通用的界面,并进行动态管理是否使用
- branch v_3.1;     增加了第三方的友盟统计
- branch v_3.1.1    增加部分功能模块界面,并更新工具类版本到2.0

**将以下代码复制到signing.properties文件中，如果没有则新建在app\目录下**
```
###### key文件所在绝对路径
KEYSTORE_FILE=C:\\Users\\Mr.Jude\\Documents\\Android\\HelloWorld.jks
###### keystore密码
KEYSTORE_PASSWORD=xxxxxx
###### alias名称
KEY_ALIAS=xxxxxx
###### key密码
KEY_PASSWORD=xxxxxx
```
## 项目目录分配
- common 放置公共类，不是项目独有的界面工具类等
- projec 放置项目独有的工具类,界面,实体类,广播类等
  - model 放置公共实体类，包括父类，方便混淆处理
  - event EventBus使用的广播类，统一放置
  - ui 项目主要界面放置在里面，包裹Fragment,Activity,Adapter等
## 项目整体解析
### 项目适配
###### API适配
- ~~6.0以后的运行时权限,隐私权限动态申请(PermissionRequestUtils)~~  
- ~~7.0相机拍摄获取URI文件路径适配(com.xmqian.app.common.utils.TestUtils.takePhoto)~~
- []7.0多窗口模式适配
- ~~8.0通知渠道(NotificationChannelUtils)~~
- []9.0HTTP请求明文限制适配
- []9.0限制访问通话记录和电话号码
- ~~9.0前台服务限制(权限申请已配置在Manifast文件中)~~
###### 屏幕适配
- [x] 全面屏，异常比例屏幕尺寸适配18.5:9
- [x] 在过渡界面SplashActivity进行“刘海屏”适配
- [ ] 9.0刘海屏适配

### 项目规范解析
- base/BaseActivity
> 只作为管理所有Activity的父类，不进行额外操作。增添功能视项目本身而定
### 第三方工具包集成规则
> 如果第三方的工具可以进行封装，就进行封装使用。工具类在utils目录下面建立对应的工具类目录。
- utils/PermissionRequestUtils
> 权限权限申请,进行权限申请，申请结果在对应Activity中使用onRequestPermissionsResult()方法接收
- utils.apiadapter.DisplayCutoutUtils
> 屏幕适配工具类
- utils.apiadapter.NotificationChannelUtils
> 通知栏消息工具类
- utils.glide.ImageLoad
> 图片加载
- utils.logger.LoggerUtil
> 日志打印工具类
- utils.GsonUtil
> Gson解析工具类
- utils.ImageCompressionUtil
> 图片压缩工具类
- utils.SelectImageUtils
> 图片选择工具类，有可能还会包含选择视频，视封装的第三方决定

### 项目资源解析
- values/colors.xml
> 存储一些基础色调，除了主题色，里面不保存有明确目的的色彩值
- values/app_colors
> 存储带有明确目的的色彩值，比如标题，问题，色彩值从colors文件中取得
- values/styles.xml
> 设置项目主题，仅限于基于AppTheme.Base继承创建界面主题
- values/dimens.xml
> 单位适配,仅在项目中的布局文件使用,需要用到大小明确的单位时，新建app_dimens文件
- values/strings.xml
> 保存通用的字符串资源，比如网络错误提示等，如果需要按模块区分字符串，新建app_dimens文件
- drawable
> 此文件夹下只放置可以做成.9图的资源图片，或者矢量图片，和shap,select资源.
资源文件命名规则，一般有明确目的的使用对应类名或者功能名进行命名。如果目的不明确，比如，按钮，输入框背景，
这些比较普遍的，就用 简称_样式_颜色_作用 进行命名。比如 sp_cir_rec_red_btn，这是方形圆角红色按钮。

完整英文 | 简写
---- | ----
select | sl
shap | sp
circle | cir
rectangle | rec
buttom | btn
background | bg
## 项目用法解析
- key信息，在app目录中的signing.properties文件中，需要填写对应的信息，配置已经在app的build.graadle文件中配置好了
- 项目配置信息，统一存放在项目根目录的config.gradle文件中，包含有构建版本，兼容库版本，项目版本号的统一处理适配
- 项目中一些功能如需关闭，可以到**com.xmqian.app.common.config.AppConfig**文件中进行关闭
- app文件夹中的build.gradle文件设置了混淆模式，默认关闭，增加了65536异常处理机制
- 项目包含颜色资源，和屏幕适配资源，使用时先查看values文件夹
- 已包含过渡界面，过渡界面风格为全屏，过渡图片存放在drawable-xhdpi文件夹中,文件名为splash_image.png,可以替换为自己的图片
其余的东西不用动，逻辑类为，SplashActivity
- 项目中已包含 **Toast**提示工具类，已在MyApplication中进行了初始化，使用直接调用 **ToastUtils.show(str)** 方法即可
- 已包含日志工具类 **LogX** ，在项目中使用时直接使用，**LogX.e()**,内部做了正式包不会打印日志的处理
- 包含SharedPreferences工具类**SharedPreUtils** 用来存储一些简单的数据类型，当然，最好是用来存储应用的设置信息，比如是否推送，
是否已登录等信息。存储文件可以被直接读取打开，所以不建议存储隐私数据
- app的设置数据保存在config.AppConfig文件中，详细规则内部有
- 项目中配置多个主页**(MainBottomTabActivity)** 底部菜单主页、**(MainActivity)**普通单个主页、**(TestMainActivity)** 测试主页,内部模块通过这个主页进行跳转
#### 开启混淆后，需要在混淆文件中配置实体类的混淆规则
- 在混淆gson的时候，需要注意将-keep class com.xxx.xxx.bean.** { *; }修改成你自己的解析类所在的包．这就要求我们的解析类最好都放在一个包．
- 在混淆rxjava的时候，需要注意将自己处理请求返回的转换类如：public class HttpResult<T>，　序列化，如下：
  public class HttpResult<T>implements Serializable {．　　这里主要是序列化后，这个类就不会被混淆，也就不会导致网络返回后，rxjava无法解析．
