package com.xmqian.app.common.config.network;

import com.coszero.utilslibrary.app.AppInfoUtils;
import com.coszero.utilslibrary.app.AppUtils;
import com.xmqian.app.project.MyApplication;

/**
 * Desc： 网络Api相关配置文件
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/5/17 14:50
 */
public class ApiFactory {
    public static final String CLIENT = "android";
    public static final String PACKAGE = AppInfoUtils.getPackageName(MyApplication.getAppContext());
    public static final String VER = AppInfoUtils.getVersionName(MyApplication.getAppContext());
    //    public static final String BASE_URL = "http://47.111.96.54:8011/api.php/";//原始
    public static final String BASE_URL = "http://47.99.40.21/api.php/";//新的
    //    public static final String BASE_URL = "http://192.168.1.188/api.php/";//新的
    public static final String SIGN_OUT = "center/Member/layout";
}
