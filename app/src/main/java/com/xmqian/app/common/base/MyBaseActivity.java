package com.xmqian.app.common.base;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.coszero.utilslibrary.base.BaseActivity;
import com.coszero.utilslibrary.utils.StringUtils;
import com.xmqian.app.R;
import com.xmqian.app.common.utils.umeng.UMengAnalyticsUtils;
import com.xmqian.app.common.widget.MyToolBar;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;


/**
 * Desc： 每个项目自定义的父类
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/6/4 19:47
 */
public abstract class MyBaseActivity extends BaseActivity {

    private MyToolBar myToolBar;
    private boolean showBack = true;
    private String mainTitle;
    protected Activity mActivity = this;

    /**
     * 设置缺口屏的显示模式,最好在onCreate中使用
     * -默认不可使用刘海区域，非全面平可以使用：MODE_DEFAULT=0
     * -允许延伸到刘海区域SHORT_EDGES=1
     * -不允许使用刘海区域NEVER=2
     */
    protected void setDisplayCutouMode(int showMode) {
        if (Build.VERSION.SDK_INT >= 28) {
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            if (showMode != 0) {
                lp.layoutInDisplayCutoutMode = showMode;
            } else {
                lp.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
            }
            getWindow().setAttributes(lp);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentLayoutRes());
        ButterKnife.bind(this);
        myToolBar = findViewById(R.id.my_toolbar);
        if (myToolBar != null) {
            if (showBack) {
                myToolBar.setNavigationIcon(R.drawable.sl_ic_arrow_back);
            }
            myToolBar.setTitle("");
            if (!StringUtils.isEmpty(mainTitle)) {
                myToolBar.setMainTitle(mainTitle);
            }
            setSupportActionBar(myToolBar);
            myToolBar.setNavigationOnClickListener(v -> finish());
        }
        initView();
    }

    protected abstract void initView();

    @Override
    protected void onResume() {
        super.onResume();
        UMengAnalyticsUtils.onResumeToActivity(this);
    }

    /**
     * @param showBack 是否显示返回键
     */
    public void setShowBack(boolean showBack) {
        this.showBack = showBack;
    }

    public void setTitle(String title) {
        if (myToolBar != null) {
            myToolBar.setMainTitle(title);
        } else {
            this.mainTitle = title;
        }
    }

    public MyToolBar getMyToolBar() {
        if (null != myToolBar) {
            return myToolBar;
        }
        return null;
    }

    /*返回主布局ID*/
    protected abstract int getContentLayoutRes();

    /**
     * 设置透明状态栏（API21，5.0之后才能用）
     *
     * @param color 通知栏颜色，完全透明填 Color.TRANSPARENT 即可
     * @param isLightMode 是否为亮色模式（黑色字体，需要6.0 以后支持，否则显示无效）
     */
    protected void requestTranslucentStatusBar(int color, boolean isLightMode) {
        //大于5.0才设置
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //大于6.0 并且是亮色模式
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && isLightMode) {
                getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            } else {
                getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            }
            //因为5.0不支持黑色字体模式，如果设置成透明色则字体回看不到，设置成半透明黑色
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP && color == Color.TRANSPARENT) {
                color = Color.parseColor("#44000000");
            }
            getWindow().setStatusBarColor(color);
        }
    }

    @Override
    protected void onPause() {
        UMengAnalyticsUtils.onPauseToActivity(this);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }
}
