package com.xmqian.app.common.testdata;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.coszero.utilslibrary.utils.LogX;
import com.coszero.utilslibrary.utils.ToastUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.xmqian.app.R;
import com.xmqian.app.common.base.MyBaseActivity;
import com.xmqian.app.common.config.Global;
import com.xmqian.app.common.utils.apiadapter.NotificationChannelUtils;
import com.xmqian.app.common.utils.apiadapter.PermissionRequestUtils;
import com.xmqian.app.common.utils.glide.ImageLoad;
import com.xmqian.app.common.utils.matisse.SelectImageUtils;
import com.xmqian.app.common.utils.xpopup.XPopupUtils;
import com.xmqian.app.project.MainActivity;
import com.xmqian.app.project.MainBottomTabActivity;
import com.xmqian.app.project.dialog.ChoosePictureDialog;
import com.xmqian.app.project.event.LoginEvent;
import com.xmqian.app.project.ui.common.activity.MyWebViewActivity;
import com.xmqian.app.project.ui.user.activity.LoginActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;


public class TestMainActivity extends MyBaseActivity {
    private static final long DELAY_TIME = 1500;//退出间隔时间
    private long lastPressBackKeyTime = 0;
    private TestUtils testUtils;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.iv_image)
    ImageView mIvImage;
    private String takePhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void initView() {
        smartRefreshLayout.setOnRefreshListener(refreshlayout -> {
            refreshlayout.finishRefresh(2000/*,false*/);//传入false表示刷新失败
        });
        smartRefreshLayout.setOnLoadMoreListener(refreshlayout -> {
            refreshlayout.finishLoadMore(2000/*,false*/);//传入false表示加载失败
        });
        testUtils = TestUtils.newInstance(this);
        String[] stringArray = getResources().getStringArray(R.array.item_name);
        final List<String> items = Arrays.asList(stringArray);
        ListView listView = findViewById(R.id.lv_list);
        listView.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, items) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView = new TextView(getContext());
                textView.setText(items.get(position));
                textView.setTextSize(16);
                textView.setBackgroundColor(0x99ffffff);
                textView.setPadding(0, 30, 0, 30);
                textView.setGravity(Gravity.CENTER);
                textView.setTypeface(Typeface.DEFAULT_BOLD);
                return textView;
            }
        });
        listView.setOnItemClickListener((parent, view, position, id) -> use(position));
        //初始化渠道通知，8.0+
        NotificationChannelUtils.newInstance(this).init();
    }

    @Override
    protected int getContentLayoutRes() {
        return R.layout.activity_test_main;
    }

    private void use(int pos) {
        switch (pos) {
            case 0://申请权限
                com.coszero.uilibrary.dialog.AlertDialog alertDialog = new com.coszero.uilibrary.dialog.AlertDialog(this);
                alertDialog.builder().setTitle("温馨提示")
                        .setMsg("使用此功能需要开启相机权限")
                        .setPositiveButton("确定开启", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PermissionRequestUtils.requestPermission(TestMainActivity.this, Manifest.permission.CAMERA, 0);
                            }
                        }).show();
                break;
            case 1://打开相机
                testUtils.takePhoto();
                break;
            case 2://渠道通知
                NotificationChannelUtils.newInstance(this).sendMessage();
                LogX.d("### 通知渠道 ---使用自定义jar");
                break;
            case 3://登录注册
                LoginActivity.startTask(this);
                break;
            case 4://通用网页
                startActivity(MyWebViewActivity.getIntent(this, "通用网页", "https://www.baidu.com"));
                break;
            case 5://头像选择弹窗
                XPopupUtils xPopupUtils = new XPopupUtils(this);
                xPopupUtils.init(new ChoosePictureDialog(this, new ChoosePictureDialog.ChoosePictureCallBack() {
                    @Override
                    public void selectGallery() {
                        SelectImageUtils.selectSingleImage(TestMainActivity.this);
                    }

                    @Override
                    public void selectCapture() {
                        SelectImageUtils.takePhoto(TestMainActivity.this, path -> {
                            takePhotoPath = path;
                        });
                    }
                })).show();
                break;
            case 6:
                MainActivity.startActivity(mActivity);
                break;
            case 7:
                MainBottomTabActivity.startTask(mActivity);
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 0://申请相机
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ToastUtils.showMsg("权限申请成功");
                } else {
                    showWaringDialog();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case TestUtils.REQUEST_TAKE_PHOTO_CODE:
                    //相机
                    String path = Global.CameraPath + File.separator + "test.jpg";
                    File file = new File(path);
                    LogX.i("### 拍摄照片后的路径：" + path + "\n 文件大小：" + file.length());
                    ImageLoad.loadImage(this, path, mIvImage);
                    break;
                case SelectImageUtils.REQUEST_CODE_CHOOSE:
                    List<String> imagePaths = SelectImageUtils.getImagePaths(data);
                    String s = imagePaths.get(0);
                    ImageLoad.loadImage(this, s, mIvImage);
                    break;
                case SelectImageUtils.REQUEST_CODE_CHOOSE_PHOTO_SHOOT:
                    String takePhotoFile = SelectImageUtils.getTakePhotoFile(takePhotoPath);
                    ImageLoad.loadImage(this, takePhotoFile, mIvImage);
                    break;
            }
        }
    }

    private void showWaringDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("警告！")
                .setMessage("请前往设置->应用->" + getString(R.string.app_name) + "->权限中打开相关权限，否则功能无法正常运行！")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // 一般情况下如果用户不授权的话，功能是无法运行的，做退出处理
//                        finish();
                    }
                }).show();
    }

    @Override
    public void onBackPressed() {
        if (System.currentTimeMillis() - lastPressBackKeyTime < DELAY_TIME) {
            super.onBackPressed();
        } else {
            lastPressBackKeyTime = System.currentTimeMillis();
            ToastUtils.showLongMsg(getResources().getString(R.string.toast_press_again_exit_app));
        }
    }

    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (System.currentTimeMillis() - lastPressBackKeyTime < DELAY_TIME) {
                return super.onKeyDown(keyCode, event);
            } else {
                lastPressBackKeyTime = System.currentTimeMillis();
                ToastUtils.showLongMsg(getResources().getString(R.string.toast_press_again_exit_app));
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }*/

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginEvent(LoginEvent event) {
        if (event.isLogin()) {
            LogX.i("### 登录成功");
        } else {
            LogX.i("### 退出登录成功");
        }
    }

}
