package com.xmqian.app.common.config.network;


import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by xuyougen on 2018/4/11.
 */

public interface ApiService {
    //获取全局配置
    @POST("center/member/getsets")
    Observable<String> getAppSets(@Body RequestBody body);

    //    ?type=1&page=1
    @GET("satinApi")
    Observable<String> getContent(@Query("type") int type, @Query("page") int page);
}
