package com.xmqian.app.common.base.mvp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;

import com.xmqian.app.common.base.MyBaseActivity;

/**
 * MVP Activity 基类
 * Created by xuyougen on 2018/4/11.
 */

public abstract class BaseMvpActivity<V extends BaseView, P extends BasePresenter<V>> extends MyBaseActivity {

    protected P mPresenter;
    //加载时需要的dialog
    private Dialog mLoadingDialog;

    //5.0以下的矢量图支持配置
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mLoadingDialog == null)
            mLoadingDialog = createProgressDialog();
        initPresenter();
        //开始进行初始化相关数据
        initData();
    }

    protected Dialog createProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle("请稍等");
        dialog.setCancelable(false);
        dialog.setIndeterminate(true);
        return dialog;
    }

    protected abstract void initData();

    protected void initPresenter() {
        if (mPresenter == null) {
            mPresenter = createPresenter();
            mPresenter.attach((V) this);
            mLoadingDialog = createProgressDialog();
        }
    }

    protected abstract P createPresenter();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detach();
    }

    public void showLoadingDialog() {
        runOnUiThread(() -> {
            if (mLoadingDialog != null && !mLoadingDialog.isShowing()) {
                mLoadingDialog.show();
            }
        });

    }

    public void hideLoadingDialog() {
        runOnUiThread(() -> {
            if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
                mLoadingDialog.dismiss();
            }
        });

    }
}
