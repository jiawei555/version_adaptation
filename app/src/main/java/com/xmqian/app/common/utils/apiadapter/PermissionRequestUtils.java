package com.xmqian.app.common.utils.apiadapter;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.coszero.utilslibrary.utils.LogX;
import com.coszero.utilslibrary.utils.ToastUtils;
import com.xmqian.app.common.callback.PermissionRequestCallBack;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.runtime.Permission;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * @author xmqian
 * @date 19-3-19
 * @desc 6.0动态权限请求，手动在所在界面使用#onRequestPermissionsResult()#进行回调
 */
public class PermissionRequestUtils {
    private PermissionRequestCallBack requestCallBack;
    public static final int REQUEST_PERMISSION_CODE = 101;
    private static final String TAG = "####PermissionRequest";
    public static final String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE
            , Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION};

    /**
     * 单权限申请,请在申请的Activity中接收回调
     *
     * @param context
     * @param permission
     * @param requestCode 请求码
     * @deprecated 原生得暂时废弃
     */
    @TargetApi(23)
    public static void requestPermission(Activity context, @NonNull String permission, int requestCode) {
        if (Build.VERSION.SDK_INT < 23) {
            LogX.i(TAG, "不需要申请权限");
            return;
        }
        // 检查危险权限是否申请
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            LogX.i(TAG, "checkSelfPermission");
            //判断用户是否拒绝过权限申请，如果拒绝过并选中不再提示，则返回false
            if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                LogX.i(TAG, "shouldShowRequestPermissionRationale");
                //申请权限，返回值使用onRequestPermissionsResult()接收
                ActivityCompat.requestPermissions(context,
                        new String[]{permission},
                        requestCode);
            } else {
                LogX.i(TAG, "requestPermissions");
                ActivityCompat.requestPermissions(context,
                        new String[]{permission},
                        requestCode);
            }
        }
    }

    /**
     * 多权限申请,手动接收回调
     * 超过三个以上建议使用第三方进行申请
     */
    public static void requestEasyPermissions(Activity context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            LogX.i(TAG, "不需要申请权限");
            return;
        }
        LogX.i("### 检查权限值为：" + ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
                + "  同意值为：" + PackageManager.PERMISSION_GRANTED);
        List<String> permissionList = new ArrayList<>();
        //这里有过多的判断，不多写了，如果多个请求请使用第三方
        if (ContextCompat.checkSelfPermission(context, permissions[0])
                != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(permissions[0]);
        }
        if (ContextCompat.checkSelfPermission(context, permissions[1])
                != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(permissions[1]);
        }
        if (ContextCompat.checkSelfPermission(context, permissions[2])
                != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(permissions[2]);
        }
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(permissions[3]);
        }
        if (permissionList.size() > 0) {
            context.requestPermissions(permissionList.toArray(new String[permissionList.size()]),
                    REQUEST_PERMISSION_CODE);
        }
    }

    /**
     * 请求简单的权限
     *
     * @param requestCallBack
     */
    public static void requestEasyPermission(Activity activity, PermissionRequestCallBack requestCallBack) {
        AndPermission.with(activity).runtime().permission(
                Permission.Group.STORAGE)
                .onGranted(data -> {
                    //申请成功
                    requestCallBack.requestSuccess();
                })
                .onDenied(data -> {
                    //申请失败
                    requestCallBack.requestFaile();
                }).start();
    }

    /**
     * 请求独立权限
     *
     * @param context
     * @param requestCallBack
     * @param permission permision1,permission2
     */
    public static void requestPermission(Context context, PermissionRequestCallBack requestCallBack, @NonNull String... permission) {
        WeakReference weakReference = new WeakReference(context);
        AndPermission.with((Activity) weakReference.get()).runtime().permission(permission)
                .onGranted(data -> {
                    //申请成功
                    requestCallBack.requestSuccess();
                })
                .onDenied(data -> {
                    //申请失败
                    ToastUtils.showMsg("权限申请失败");
                    requestCallBack.requestFaile();
                }).start();
    }

    /**
     * 请求权限组
     *
     * @param context
     * @param requestCallBack
     * @param permissionGroups [],[]
     */
    public static void requestPermission(Context context, PermissionRequestCallBack requestCallBack, String[]... permissionGroups) {
        WeakReference weakReference = new WeakReference(context);
        AndPermission.with((Activity) weakReference.get()).runtime().permission(permissionGroups)
                .onGranted(data -> {
                    //申请成功
                    requestCallBack.requestSuccess();
                })
                .onDenied(data -> {
                    //申请失败
                    requestCallBack.requestFaile();
                }).start();
    }

    /**
     * 安装未知来源应用，需要在清单文件中申请 REQUEST_INSTALL_PACKAGES
     *
     * @param activity
     * @param apkPath
     */
    public static void installApk(Activity activity, String apkPath) {
        File file = new File(apkPath);
        AndPermission.with(activity)
                .install()
                .file(file)
                .start();
    }
}
