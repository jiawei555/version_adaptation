package com.xmqian.app.common.utils.umeng;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.text.TextUtils;


import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;
import com.xmqian.app.common.config.Global;

import java.lang.reflect.Method;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Locale;

/**
 * @author xmqian
 * @date 2018/8/4 0004
 * @desc 友盟统计相关配置方法
 */
public class UMengAnalyticsUtils {

    /**
     * 在Application中做的初始化
     */
    public static void initUmeng(Context context, String appKey) {
        //日志开关
        UMConfigure.setLogEnabled(Global.IS_DEBUG_VERSION);
        //开启调试模式（如果不开启debug运行不会上传umeng统计）
        MobclickAgent.setDebugMode(false);
        //日志加密
        UMConfigure.setEncryptEnabled(false);
        // isEnable: false-关闭错误统计功能；true-打开错误统计功能（默认打开）
        MobclickAgent.setCatchUncaughtExceptions(!Global.IS_DEBUG_VERSION);
        //子进程是否支持自定义事件统计
        UMConfigure.setProcessEvent(false);
        //初始化配置信息参数1:上下文，不能为空
        //* 参数2:【友盟+】 AppKey
        //* 参数3:【友盟+】 Channel
        //* 参数4:设备类型，UMConfigure.DEVICE_TYPE_PHONE为手机、UMConfigure.DEVICE_TYPE_BOX为盒子，默认为手机
        //* 参数5:Push推送业务的secretPush推送业务的secret，需要集成Push功能时必须传入Push的secret，否则传空
        UMConfigure.init(context, appKey, "Android", UMConfigure.DEVICE_TYPE_PHONE, "");
        // 选用AUTO页面采集模式
        MobclickAgent.setPageCollectionMode(MobclickAgent.PageMode.LEGACY_AUTO);
    }

    /**
     * 在BaseActivity跟BaseFragmentActivity中的onResume加入
     *
     * @param context
     */
    public static void onResumeToActivity(Context context) {
        MobclickAgent.onPageStart(context.getClass().getName());
        MobclickAgent.onResume(context);
    }

    /**
     * 在BaseActivity跟BaseFragmentActivity中的onPause加入
     *
     * @param context
     */
    public static void onPauseToActivity(Context context) {
        MobclickAgent.onPageEnd(context.getClass().getName());
        MobclickAgent.onPause(context);
    }

    /**
     * 在BaseFragment中的onResume加入
     *
     * @param viewName 页面名称
     */
    public static void onResumeToFragment(String viewName) {
        MobclickAgent.onPageStart(viewName);
    }

    /**
     * 在BaseFragment中的onPause加入
     *
     * @param viewName 页面名称
     */
    public static void onPauseToFragment(String viewName) {
        MobclickAgent.onPageEnd(viewName);
    }

    /**
     * 账号统计
     * 在登录成功的地方调用
     *
     * @param userId 用户id
     */
    public static void onLogin(String userId) {
        MobclickAgent.onProfileSignIn(userId);
    }

    /**
     * @param Provider 账号来源。如果用户通过第三方账号登陆，可以调用此接口进行统计。支持自定义，
     * 不能以下划线”_”开头，使用大写字母和数字标识，长度小于32 字节; 如果是上市
     * 公司，建议使用股票代码。
     * @param ID
     */
    public static void onLogin(String Provider, String ID) {
        MobclickAgent.onProfileSignIn(Provider, ID);
    }

    /**
     * 在退出登录的地方调用
     * 结束账号统计内容
     */
    public static void onLogout() {
        MobclickAgent.onProfileSignOff();
    }


    public static String getDeviceInfo(Context context) {
        try {
            org.json.JSONObject json = new org.json.JSONObject();
            android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            String device_id = null;
            if (checkPermission(context, Manifest.permission.READ_PHONE_STATE)) {
                device_id = tm.getDeviceId();
            }
            String mac = getMac(context);

            json.put("mac", mac);
            if (TextUtils.isEmpty(device_id)) {
                device_id = mac;
            }
            if (TextUtils.isEmpty(device_id)) {
                device_id = android.provider.Settings.Secure.getString(context.getContentResolver(),
                        android.provider.Settings.Secure.ANDROID_ID);
            }
            json.put("device_id", device_id);
            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getMac(Context context) {
        String mac = "";
        if (context == null) {
            return mac;
        }
        if (Build.VERSION.SDK_INT < 23) {
            mac = getMacBySystemInterface(context);
        } else {
            mac = getMacByJavaAPI();
            if (TextUtils.isEmpty(mac)) {
                mac = getMacBySystemInterface(context);
            }
        }
        return mac;

    }

    @TargetApi(9)
    private static String getMacByJavaAPI() {
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface netInterface = interfaces.nextElement();
                if ("wlan0".equals(netInterface.getName()) || "eth0".equals(netInterface.getName())) {
                    byte[] addr = netInterface.getHardwareAddress();
                    if (addr == null || addr.length == 0) {
                        return null;
                    }
                    StringBuilder buf = new StringBuilder();
                    for (byte b : addr) {
                        buf.append(String.format("%02X:", b));
                    }
                    if (buf.length() > 0) {
                        buf.deleteCharAt(buf.length() - 1);
                    }
                    return buf.toString().toLowerCase(Locale.getDefault());
                }
            }
        } catch (Throwable e) {
        }
        return null;
    }

    private static String getMacBySystemInterface(Context context) {
        if (context == null) {
            return "";
        }
        try {
            WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (checkPermission(context, Manifest.permission.ACCESS_WIFI_STATE)) {
                WifiInfo info = wifi.getConnectionInfo();
                return info.getMacAddress();
            } else {
                return "";
            }
        } catch (Throwable e) {
            return "";
        }
    }

    public static boolean checkPermission(Context context, String permission) {
        boolean result = false;
        if (context == null) {
            return result;
        }
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                Class<?> clazz = Class.forName("android.content.Context");
                Method method = clazz.getMethod("checkSelfPermission", String.class);
                int rest = (Integer) method.invoke(context, permission);
                if (rest == PackageManager.PERMISSION_GRANTED) {
                    result = true;
                } else {
                    result = false;
                }
            } catch (Throwable e) {
                result = false;
            }
        } else {
            PackageManager pm = context.getPackageManager();
            if (pm.checkPermission(permission, context.getPackageName()) == PackageManager.PERMISSION_GRANTED) {
                result = true;
            }
        }
        return result;
    }

    /**
     * 上传错误日志到友盟
     *
     * @param context
     * @param log
     */
    public static void uploadError(Context context, String log) {
        MobclickAgent.reportError(context, log);
    }

    public static void uploadError(Context context, Exception log) {
        MobclickAgent.reportError(context, log);
    }
}
