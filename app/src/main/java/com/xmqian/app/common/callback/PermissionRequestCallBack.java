package com.xmqian.app.common.callback;

/**
 * Desc： 权限请求回调
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/8/1 15:19
 */
public interface PermissionRequestCallBack {
    void requestSuccess();//请求成功

    void requestFaile();//请求失败
}
