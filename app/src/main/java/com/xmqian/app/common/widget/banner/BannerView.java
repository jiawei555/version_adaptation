package com.xmqian.app.common.widget.banner;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.coszero.uilibrary.banner.widget.Banner.BaseIndicatorBanner;
import com.coszero.utilslibrary.phone.PhoneScreenInfoUtils;
import com.xmqian.app.common.utils.glide.ImageLoad;
import com.xmqian.app.project.model.BannerBean;

public class BannerView extends BaseIndicatorBanner<BannerBean, BannerView> {
    public BannerView(Context context) {
        super(context);
    }

    public BannerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BannerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onTitleSlect(TextView tv, int position) {
        //设置标题
    }

    @Override
    public View onCreateItemView(int position) {
        int screenWidth = PhoneScreenInfoUtils.getScreenWidth(getContext());
        BannerBean bannerBean = mDatas.get(position);
        ImageView iv = new ImageView(getContext());
        iv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
        String imgUrl = bannerBean.getPic();
//        Glide.with(mContext).load(imgUrl).into(iv);
        ImageLoad.loadImage(getContext(), imgUrl, iv);
        return iv;
    }
}
