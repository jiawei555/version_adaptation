package com.xmqian.app.common.utils.logger;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.LogStrategy;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;
import com.xmqian.app.BuildConfig;

/**
 * Desc:第三方Logger的封装类
 * <p>com.orhanobut:logger:2.1.1
 * link:https://github.com/orhanobut/logger
 * Author xmqian
 * Email:xmqian93@163.com
 * Date: 2019/5/17
 */
public class LoggerUtil {
    public static void initLogger(String appName) {
        PrettyFormatStrategy strategy = PrettyFormatStrategy.newBuilder()
                .logStrategy(new LogStrategy() {
                    private int last;

                    private String randomKey() {
                        int random = (int) (10 * Math.random());
                        if (random == last) {
                            random = (random + 1) % 10;
                        }
                        last = random;
                        return String.valueOf(random);
                    }

                    @Override
                    public void log(int priority, @Nullable String tag, @NonNull String message) {
                        Log.println(priority, randomKey() + " " + tag, message);
                    }
                })
                .showThreadInfo(false)
                .methodCount(1)
                .tag(appName)
                .build();
        Logger.addLogAdapter(new AndroidLogAdapter(strategy) {
            @Override
            public boolean isLoggable(int priority, String tag) {
                return BuildConfig.DEBUG;
            }
        });
    }

    public static void e(String tag, String msg) {
        Logger.t(tag).e(msg);
    }
}
