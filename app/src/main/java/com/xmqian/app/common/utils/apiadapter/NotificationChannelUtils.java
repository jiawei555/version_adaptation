package com.xmqian.app.common.utils.apiadapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.xmqian.app.R;

import java.lang.ref.WeakReference;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * @author Administrator
 * @date 2019/4/23
 * @desc Android 8.0渠道消息配置
 */
public class NotificationChannelUtils {
    Context myActivity;
    WeakReference weakReference;

    public NotificationChannelUtils(Activity activity) {
        weakReference = new WeakReference(activity);
        myActivity = (AppCompatActivity) weakReference.get();
    }

    public static NotificationChannelUtils newInstance(Activity activity) {
        NotificationChannelUtils channelUtils = new NotificationChannelUtils(activity);
        return channelUtils;
    }

    @TargetApi(26)
    public void init() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //创建渠道组
            createNotifycationGroup("chat", "聊天");
            createNotifycationGroup("subs", "订阅");

            String channelId = "chat";
            String channelName = "聊天消息";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            createNotificationChannel(channelId, "chat", channelName, importance);
            channelId = "subscribe";
            channelName = "订阅消息";
            importance = NotificationManager.IMPORTANCE_DEFAULT;
            createNotificationChannel(channelId, "subs", channelName, importance);
        }
    }

    /**
     * 创建渠道通知
     *
     * @param channelId 渠道id
     * @param groupId 渠道组id
     * @param channelName 渠道名称
     * @param importance 通知级别
     */
    @TargetApi(Build.VERSION_CODES.O)
    private void createNotificationChannel(String channelId, String groupId, String channelName, int importance) {
        NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
        if (!TextUtils.isEmpty(groupId)) {
            channel.setGroup(groupId);
        }
        NotificationManager notificationManager = (NotificationManager) myActivity.getSystemService(
                NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(channel);
        }
    }

    /*不使用渠道组功能使用此方法*/
    @TargetApi(26)
    private void createNotificationChannel(String channelId, String channelName, int importance) {
        createNotificationChannel(channelId, "", channelName, importance);
    }


    /**
     * 创建一个渠道组
     *
     * @param groupId
     * @param groupName
     */
    @TargetApi(26)
    private void createNotifycationGroup(String groupId, String groupName) {
        NotificationChannelGroup group = new NotificationChannelGroup(groupId, groupName);
        NotificationManager notificationManager = (NotificationManager) myActivity.getSystemService(
                NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.createNotificationChannelGroup(group);
        }
    }


    public void sendMessage() {
        NotificationManager manager = (NotificationManager) myActivity.getSystemService(NOTIFICATION_SERVICE);
        Notification notification = new NotificationCompat.Builder(myActivity, "chat")
                .setContentTitle("you girlfrind")
                .setContentText("今晚上回家吗?")
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_notice)
                .setLargeIcon(BitmapFactory.decodeResource(myActivity.getResources(), R.drawable.ic_notice))
                .setAutoCancel(true)
                .build();
        manager.notify(1, notification);
    }
}
