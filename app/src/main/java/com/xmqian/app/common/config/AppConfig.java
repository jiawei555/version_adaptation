package com.xmqian.app.common.config;

/**
 * @author xmqian
 * @date 2018/12/29 10:25
 * @desc app设置的静态常量，控制项目内的配置，比如是否为DEBUG模式,运用服务器版本
 */
public class AppConfig {
    /*是否延迟过渡动画*/
    public static final boolean DELAY_SPLASH = false;
    /*是否进入引导页，false直接进入登录*/
    public static final boolean INTO_GUIDE = false;
    /*是否需要先登录再使用*/
    public static final boolean NEED_FRIST_LOGIN = false;
}
