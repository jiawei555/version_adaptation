package com.xmqian.app.common.testdata;

import com.xmqian.app.project.model.BannerBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Desc： 专门用来获取测试数据的
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/8/21 15:33
 */
public class TestData {
    public static final String longText = "向江南折过花，对春风与红蜡。多情总似我，风流爱天下。人世肯相逢，醉眼万斗红沙";
    public static final String shortText = "君何归所处，静楼台，思";
    public static final String[] imageUrls = new String[]{
            "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1566799817816&di=8010225863a6b259888e629a1f3ec505&imgtype=0&src=http%3A%2F%2Fm.tuniucdn.com%2Ffb2%2Ft1%2FG1%2FM00%2FF5%2F0A%2FCii9EFbAGBiIYRv1AAB1HLdGA4YAAB2dgO53cAAAHU012_w0_h600_c0_t0.jpeg",
            "https://timgsa.baidu.com/timg?image&quality=80&size=b10000_10000&sec=1566789803&di=4fb628e572b38eaca732152016837654&src=http://imgs.qqzhiu.com/ViewPortFile/1/20160326/20160326021725479.jpg",
            "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1566799921475&di=ce8b714d3416f7575a10c1a6f75768dc&imgtype=0&src=http%3A%2F%2Fb.hiphotos.baidu.com%2Fimage%2Fpic%2Fitem%2F908fa0ec08fa513db777cf78376d55fbb3fbd9b3.jpg"};
    public static final String headImage = "http://5b0988e595225.cdn.sohucs.com/images/20171229/f2c51645c6ab44788b4bb69c80e13765.jpeg";

    public static List<String> getImages(int size) {
        List<String> strings = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            strings.add(imageUrls[size % 3]);
        }
        return strings;
    }

    public static List getListSize(int size) {
        List list = new ArrayList();
        for (int i = 0; i < size; i++) {
            list.add("这是第" + i + "个文字条目");
        }
        return list;
    }

    public static List<BannerBean> getBanners() {
        List<BannerBean> bannerBeans = new ArrayList<>();
        bannerBeans.add(new BannerBean(imageUrls[0]));
        bannerBeans.add(new BannerBean(imageUrls[1]));
        bannerBeans.add(new BannerBean(imageUrls[2]));
        return bannerBeans;
    }
}
