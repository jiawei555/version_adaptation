package com.xmqian.app.common.utils.glide;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.coszero.utilslibrary.utils.LogX;
import com.xmqian.app.R;

import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * Desc: 图片加载工具,使用GlideV4.8版本
 * <p>
 * Author xmqian
 * Email:xmqian93@163.com
 * Date: 2019/5/17
 */
public class ImageLoad {
    private static RequestOptions options = new RequestOptions()
            .centerCrop().skipMemoryCache(false).diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .placeholder(R.color.gray_ccc).error(R.color.gray_ccc)
            .fallback(R.color.gray_ccc).dontAnimate();

    /**
     * 正常加载图片，没有任何修饰操作
     *
     * @param context
     * @param url
     * @param iv
     */
    public static <T> void loadImage(Context context, T url, ImageView iv) {
        if (context != null) {
            RequestOptions mOption = new RequestOptions().placeholder(R.color.gray_ccc).error(R.color.gray_ccc)
                    .fallback(R.color.gray_ccc);
            Glide.with(context)
                    .load(url)
                    .apply(mOption)
                    .into(iv);
        }
    }

    /**
     * @param context
     * @param imageSource
     * @param iv
     * @param requestListener 图片加载回调
     * @param <T>
     */
    public static <T> void loadImage(Context context, T imageSource, ImageView iv, @Nullable RequestListener requestListener) {
        if (context == null) {
            LogX.w("####ImageLoad", "带加载回调的 context is null");
            return;
        }
        Glide.with(context)
                .load(imageSource)
                .apply(new RequestOptions().centerCrop())
                .addListener(requestListener)
                .into(iv);
    }

    /**
     * 加载模糊图片
     */
    private static <T> void loadBlurImage(Context context, T url, ImageView iv) throws Exception {
        if (context != null) {
            RequestOptions blurOptions = new RequestOptions()
                    .error(R.color.gray_ccc)
                    .placeholder(R.color.gray_ccc)
                    .fallback(R.color.gray_ccc)
                    .bitmapTransform(new BlurTransformation(25, 3));
            Glide.with(context)
                    .load(url)
                    .apply(blurOptions)
                    .into(iv);
        }
    }

    /**
     * 加载本地图片
     *
     * @param context
     * @param drawable
     * @param iv
     */
    public static void loadLocalImage(Context context, int drawable, ImageView iv) {
        if (context != null) {
            Glide.with(context).load(drawable).into(iv);
        }
    }

    /**
     * 加载本地图片
     * 不做任何处理，增加大小限制
     *
     * @param context
     * @param drawable
     * @param iv
     */
    public static void loadLocalImage(Context context, int drawable, ImageView iv, int width, int height) {
        if (context != null) {
            options.override(width, height);
            Glide.with(context).load(drawable).apply(options).into(iv);
        }
    }

    /**
     * 头像加载专用，已设置默认图
     *
     * @param context
     * @param url
     * @param imageView
     * @param <T>
     */
    public static <T> void loadHead(Context context, T url, ImageView imageView) {
        if (context != null) {
            options.placeholder(R.drawable.ic_default_head).error(R.drawable.ic_default_head)
                    .fallback(R.drawable.ic_default_head);
            Glide.with(context).load(url).apply(options).into(imageView);
        }
    }

    public static <T> void loadHead(Context context, T url, ImageView imageView, int height, int width) {
        if (context != null) {
            options.placeholder(R.drawable.ic_default_head).error(R.drawable.ic_default_head)
                    .fallback(R.drawable.ic_default_head).override(height, width);
            Glide.with(context).load(url).apply(options).into(imageView);
        }
    }

    /**
     * 闪屏界面加载图片，需要传入标签
     *
     * @param context
     * @param url
     * @param imageView
     * @param sign 签名：改变签名能使同一连接的图片保持改变
     * @param <T>
     */
    public static <T> void loadSplashImage(Context context, T url, ImageView imageView, String sign) {
        if (context == null) return;
        options.signature(new ObjectKey(sign));
        Glide.with(context)
                .load(url)
                .thumbnail(0.1f)
                .apply(options)
                .into(imageView);
    }

    /**
     * 非正常加载图片，通过缩略图，限制大小,跳过内存来进行内存优化
     *
     * @param context
     * @param url
     * @param iv 广场
     */
    public static void loadPlaza(Context context, String url, ImageView iv, int width, int height) throws Exception {
        if (context != null) {
            options.override(width, height).centerCrop();
            Glide.with(context)
                    .load(url)
                    .apply(options)
                    .thumbnail(1.0f)
                    .into(iv);
        }
    }

    /**
     * 加载Banner
     *
     * @param context
     * @param url
     */
    public static void loadBanner(Context context, String url, ImageView iv, int width, int height) {
        if (context != null) {
            options.override(width, height).centerCrop();
            Glide.with(context)
                    .load(url)
                    .into(iv);
        }
    }

    /**
     * 加载圆角图片
     *
     * @param context
     * @param url
     * @param iv
     */
    public static void loadRoundImage(Context context, String url, ImageView iv) {
        if (context != null) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.bitmapTransform(new CircleCrop());
            Glide.with(context)
                    .load(url)
                    .apply(options)
                    .into(iv);
        }
    }
}
