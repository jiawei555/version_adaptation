package com.xmqian.app.common.utils.xpopup;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.core.DrawerPopupView;
import com.lxj.xpopup.core.ImageViewerPopupView;
import com.lxj.xpopup.core.PositionPopupView;
import com.lxj.xpopup.enums.PopupAnimation;
import com.lxj.xpopup.enums.PopupPosition;
import com.lxj.xpopup.impl.AttachListPopupView;
import com.lxj.xpopup.impl.PartShadowPopupView;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.lxj.xpopup.interfaces.OnSrcViewUpdateListener;
import com.xmqian.app.R;

import java.lang.ref.WeakReference;
import java.util.List;

public class XPopupUtils {
    private Context context;
    WeakReference weakReference;

    public XPopupUtils(Context context) {
        this.context = context;
        weakReference = new WeakReference(context);
    }

    /**
     * @param basePopupView 中间弹窗：CenterPopupView
     * 底部弹窗：BottomPopupView
     * 全屏弹窗：FullScreenPopupView
     * 自定义Image弹窗：ImageViewerPopupView
     * @param <T>
     * @return
     */
    public <T extends BasePopupView> T init(BasePopupView basePopupView) {
        return (T) new XPopup.Builder((Context) weakReference.get()).asCustom(basePopupView);
    }

    public XPopup.Builder init() {
        return new XPopup.Builder((Context) weakReference.get());
    }

    /**
     * 局部阴影弹窗：PartShadowPopupView
     *
     * @param basePopupView
     * @param view 需要设置依赖的View
     * @param <T>
     * @return
     */
    public <T extends PartShadowPopupView> T init(PartShadowPopupView basePopupView, View view) {
        return (T) new XPopup.Builder((Context) weakReference.get()).atView(view).asCustom(basePopupView);
    }

    /**
     * 自定义Position弹窗
     *
     * @param basePopupView
     * @param ofx x定位 ，如果需要居中，设置isCenter为true,y为-1即可
     * @param ofy y定位
     * @param isCenterHorizontal
     * @param <T>
     * @return
     */
    public <T extends PositionPopupView> T init(@NonNull PositionPopupView basePopupView, int ofx, int ofy, boolean isCenterHorizontal) {
        XPopup.Builder builder = new XPopup.Builder((Context) weakReference.get())
                .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                .isCenterHorizontal(isCenterHorizontal)
                .offsetY(ofy);
        if (ofx != -1) {
            builder.offsetX(ofx);
        }
        return (T) builder.asCustom(basePopupView);
    }

    /**
     * 使用自定义的DrawerLayout弹窗,抽屉弹窗，从左或右弹出
     *
     * @param basePopupView
     * @param ori 展示方向，PopupPosition.Right
     * @param showShadow 启用状态栏阴影
     * @param <T>
     * @return
     */
    public <T extends DrawerPopupView> T init(@NonNull DrawerPopupView basePopupView, PopupPosition ori, boolean showShadow) {
        XPopup.Builder builder = new XPopup.Builder((Context) weakReference.get())
                .popupPosition(ori)//右边
                .hasStatusBarShadow(showShadow); //启用状态栏阴影
        return (T) builder.asCustom(basePopupView);
    }

    /**
     * 展示多张图片的弹窗
     * 多图片场景
     * srcView参数表示你点击的那个ImageView，动画从它开始，结束时回到它的位置。
     * 注意：如果你自己的ImageView的scaleType是centerCrop类型的，你加载图片需要指定Original_Size，禁止Glide裁剪图片。
     *
     * @param imageView RecyclerView中展示图片的控件
     * @param pos 位置
     * @param list 图片链接列表
     * @param recyclerView 显示图片的RecyclerView
     * @param <T>
     * @return
     */
    public <T extends BasePopupView> T getImagesPopup(ImageView imageView, int pos, List list, RecyclerView recyclerView) {
        // 多图片场景
        //srcView参数表示你点击的那个ImageView，动画从它开始，结束时回到它的位置。
        //注意：如果你自己的ImageView的scaleType是centerCrop类型的，你加载图片需要指定Original_Size，禁止Glide裁剪图片。
        ImageViewerPopupView imageViewerPopupView = new XPopup.Builder((Context) weakReference.get()).asImageViewer(imageView, pos, list, new OnSrcViewUpdateListener() {
            @Override
            public void onSrcViewUpdate(ImageViewerPopupView popupView, int position) {
                // 作用是当Pager切换了图片，需要更新源View
                popupView.updateSrcView(recyclerView.getChildAt(position).findViewById(R.id.image_view));
            }
        }, new XPopupImageLoader());
        return (T) imageViewerPopupView;
    }

    /**
     * 单张图片场景
     *
     * @param imageView
     * @param url
     * @param <T>
     * @return
     */
    public <T extends BasePopupView> T getImagePopup(ImageView imageView, String url) {
        ImageViewerPopupView imageViewerPopupView = new XPopup.Builder((Context) weakReference.get())
                .asImageViewer(imageView, url, new XPopupImageLoader());
        return (T) imageViewerPopupView;
    }

    /**
     * 局部显示弹窗列表
     *
     * @param view 依附的View
     * @param titles 显示的文字
     * @param iconIds 是否带图标
     * @param listener 监听
     * @param <T>
     * @return
     */
    public <T extends BasePopupView> T getAttachListPopup(View view, String[] titles, int[] iconIds, OnSelectListener listener) {
        AttachListPopupView attachListPopupView = new XPopup.Builder((Context) weakReference.get())
                .atView(view)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                .asAttachList(titles,
                        iconIds,
                        listener);
        return (T) attachListPopupView;
    }

}
