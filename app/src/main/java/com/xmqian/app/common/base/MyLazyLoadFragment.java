package com.xmqian.app.common.base;

import android.view.View;

/**
 * Desc： 懒加载Fragment,普通的隐藏显示
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/6/4 19:51
 */
public abstract class MyLazyLoadFragment extends MyBaseFragment {
    private boolean isDataLoaded;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

    }

    @Override
    public View getView() {
        return null;
    }

    @Override
    protected void initView() {

    }

    private void getLazyData() {
        if (getUserVisibleHint() && getView() != null) {
            if (!isDataLoaded) {
                lazyLoadData();
                isDataLoaded = true;
            }
        }
    }

    public abstract void lazyLoadData();

}
