package com.xmqian.app.common.config;

/**
 * @author xmqian
 * @date 2018/12/29 10:29
 * @desc 存储静态常量，不会进行更改的
 */
public class Constant {
    /**
     * 跳转类型：
     * 0：应用第一次安装启动，进入引导界面->进入登录界面
     * 1：已执行引导界面，进入登录界面
     * 2：已执行引导界面并且已登录，进入主界面
     */
    public static final String JUMP_TYPE = "jumpType";
    /**
     * 已执行完引导功能，并且用户点击了确定进入app
     */
    public static final int GUIDE_FINISH = 1;
    /**
     * 用户已经登录完成
     */
    public static final int LOGIN_FINISH = 2;
    /**
     * 用户点击退出登录，或者异常退出
     */
    public static final int LOGIN_OUT = 1;

    /**
     * 使用的传输数值的常量
     */
    public static final String EXTRA_TITLE = "title";
}
