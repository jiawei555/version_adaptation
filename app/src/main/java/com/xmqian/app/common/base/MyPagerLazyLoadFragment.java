package com.xmqian.app.common.base;

/**
 * Desc： ViewPager的懒加载
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/6/4 19:46
 */
public abstract class MyPagerLazyLoadFragment extends MyBaseFragment {
    private boolean isDataLoaded;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        getLazyData();
    }

    private void getLazyData() {
        if (getUserVisibleHint() && getView() != null) {
            if (!isDataLoaded) {
                lazyLoadData();
                isDataLoaded = true;
            }
        }
    }

    public abstract void lazyLoadData();
}
