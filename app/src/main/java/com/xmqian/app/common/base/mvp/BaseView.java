package com.xmqian.app.common.base.mvp;

/**
 * Created by xuyougen on 2018/4/11.
 */

public interface BaseView {
    void showLoadingView();
    void hideLoadingView();
    void onNetworkError();
}
