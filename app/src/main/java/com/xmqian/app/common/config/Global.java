package com.xmqian.app.common.config;

import android.Manifest;
import android.os.Environment;

import com.xmqian.app.BuildConfig;

import java.io.File;

/**
 * @author xmqian
 * @date 2018/12/27 17:17
 * @desc 项目中会使用的常量
 */
public class Global {
    /*友盟key*/
    public static final String UM_KEY = "5cff6c434ca357f2760004e7";
    //过渡界面图片链接
    public static final String SPLASH_IMAGE_URL = "https://images.qutuo.net/square/c5848b96b0b887372e91fe41d04fef6d.jpg";
    //是否为测试服务器
    public static final boolean IS_DEBUG_SERVER = true;
    //是否为测试版本
    public static final boolean IS_DEBUG_VERSION = BuildConfig.DEBUG;
    /*权限申请列表*/
    String[] permissions = new String[]{Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET};
    /*拍照存储的路径，如果是7.0以上手机，需要和xml的文件路径同步*/
    public static final String CameraPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + File.separator + "Pictures";

}
