package com.xmqian.app.common.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.coszero.utilslibrary.utils.LogX;
import com.coszero.utilslibrary.utils.StringUtils;
import com.xmqian.app.R;

/**
 * Desc： 对Toolbar进行简单定制，主要是因为默认标题不居中
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/6/6 10:19
 */
public class MyToolBar extends Toolbar {

    /**
     * 左侧Title
     */
    private AppCompatTextView mTxtLeftTitle;
    private AppCompatImageView mIvLeftIcon;
    private FrameLayout mFlyLeftTitle;
    /**
     * 中间Title
     */
    private TextView mTxtMiddleTitle;
    /**
     * 右侧Title
     */
    private AppCompatTextView mTxtRightTitle;
    private AppCompatImageView mIvRightIcon;
    private FrameLayout mFlyRightTitle;

    public MyToolBar(Context context) {
        this(context, null);
    }

    public MyToolBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public MyToolBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        try {
            mTxtLeftTitle = findViewById(R.id.txt_left_title);
            mTxtMiddleTitle = findViewById(R.id.txt_main_title);
            mTxtRightTitle = findViewById(R.id.txt_right_title);
            mIvLeftIcon = findViewById(R.id.iv_left_icon);
            mFlyLeftTitle = findViewById(R.id.fly_left_title);
            mIvRightIcon = findViewById(R.id.iv_right_icon);
            mFlyRightTitle = findViewById(R.id.fly_right_title);
        } catch (NullPointerException e) {
            LogX.e("布局异常，控件没有找到 请设置嵌套控件");
        }
    }

    //设置中间title的内容
    public void setMainTitle(String text) {
        this.setTitle("");
        mTxtMiddleTitle.setText(text);
        mTxtMiddleTitle.setVisibility(View.VISIBLE);
    }

    //设置中间title的内容文字的颜色
    public void setMainTitleColor(int color) {
        mTxtMiddleTitle.setTextColor(color);
    }

    //设置title左边文字
    public void setLeftTitleText(String text) {
        if (!StringUtils.isEmpty(text)) {
            mTxtLeftTitle.setText(text);
            mTxtLeftTitle.setVisibility(View.VISIBLE);
        } else {
            mTxtLeftTitle.setVisibility(GONE);
        }
    }

    //设置title左边文字颜色
    public void setLeftTitleColor(int color) {
        mTxtLeftTitle.setTextColor(color);
    }

    //设置title左边图标
    public void setLeftTitleDrawable(int res, boolean isLeft) {
        Drawable dwLeft = ContextCompat.getDrawable(getContext(), res);
        dwLeft.setBounds(0, 0, dwLeft.getMinimumWidth(), dwLeft.getMinimumHeight());
        if (isLeft) {
            mTxtLeftTitle.setCompoundDrawables(dwLeft, null, null, null);
        } else {
            mTxtLeftTitle.setCompoundDrawables(null, null, dwLeft, null);
        }
    }

    //设置title左边点击事件
    public void setLeftTitleClickListener(OnClickListener onClickListener) {
        mFlyLeftTitle.setOnClickListener(onClickListener);
    }

    //设置title右边文字
    public void setRightTitleText(String text) {
        if (!StringUtils.isEmpty(text)) {
            mTxtRightTitle.setText(text);
            mTxtRightTitle.setVisibility(View.VISIBLE);
        } else {
            mTxtRightTitle.setVisibility(GONE);
        }
    }

    //设置title右边文字颜色
    public void setRightTitleColor(int color) {
        mTxtRightTitle.setTextColor(color);
    }

    //设置title右边图标
    public void setRightTitleDrawable(int res) {
        Drawable dwRight = ContextCompat.getDrawable(getContext(), res);
        dwRight.setBounds(0, 0, dwRight.getMinimumWidth(), dwRight.getMinimumHeight());
        mTxtRightTitle.setCompoundDrawables(null, null, dwRight, null);
    }

    //设置title右边点击事件
    public void setRightTitleClickListener(OnClickListener onClickListener) {
        mFlyRightTitle.setOnClickListener(onClickListener);
    }

    public void setLeftIconDrawable(int res) {
        mIvLeftIcon.setVisibility(VISIBLE);
        mIvLeftIcon.setImageResource(res);
    }

    public void setRightIconDrawable(int res) {
        mIvRightIcon.setVisibility(VISIBLE);
        mIvRightIcon.setImageResource(res);
    }
}
