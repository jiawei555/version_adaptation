package com.xmqian.app.common.widget.banner;

import com.coszero.uilibrary.banner.anim.unselect.NoAnimExist;
import com.coszero.uilibrary.banner.transform.DepthTransformer;
import com.xmqian.app.project.model.BannerBean;

import java.util.List;

public class BannerUtils {
    /**
     * @param bannerView
     * @param bannerBeans 正确的泛型应该为：BannerBean
     */
    public static void startBanner(BannerView bannerView, List<BannerBean> bannerBeans) {
        bannerView.setSelectAnimClass(NoAnimExist.class).setSource(bannerBeans).setTransformerClass(DepthTransformer.class).startScroll();
    }
}
