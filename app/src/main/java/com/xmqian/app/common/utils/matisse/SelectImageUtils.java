package com.xmqian.app.common.utils.matisse;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;

import com.coszero.utilslibrary.utils.LogX;
import com.coszero.utilslibrary.utils.ToastUtils;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.SelectionCreator;
import com.zhihu.matisse.filter.Filter;
import com.zhihu.matisse.internal.entity.CaptureStrategy;
import com.zhihu.matisse.internal.utils.MediaStoreCompat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Desc： 图片选择器封装
 * <p> 记得在 onActivityResult()方法中接收回调数据
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/8/2 18:30
 */
public class SelectImageUtils {
    public static final int REQUEST_CODE_CHOOSE = 11;//选择照片返回
    public static final int REQUEST_CODE_CHOOSE_PHOTO_SHOOT = 22;//拍照返回

    /**
     * 多张图片选择
     *
     * @param activity
     * @param maxNum 最大选择数量
     * @param chooseType 选择类型
     * @param useTakePhoto 是否使用拍照功能
     */
    private static void selectImages(Activity activity, int maxNum, Set<MimeType> chooseType, boolean useTakePhoto) {
        SelectionCreator selectionCreator = Matisse.from(activity)
                .choose(chooseType)//选择类型
                .countable(true)//是否开启数量标记
                .maxSelectable(maxNum)//最大数量
                .addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
//                .gridExpectedSize(activity.getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .thumbnailScale(0.85f)
                .imageEngine(new MyGlide4Engine());
        if (useTakePhoto) {
            selectionCreator.capture(true).captureStrategy(new CaptureStrategy(true, "com.xmqian.application"));
        }
        selectionCreator
                .forResult(REQUEST_CODE_CHOOSE);//请求码
    }


    /**
     * 图片路径 同样视频地址也是这个 根据requestCode
     *
     * @param data
     * @return
     */
    public static List<String> getImagePaths(Intent data) {
        return Matisse.obtainPathResult(data);
    }

    /**
     * @param path
     * @return
     */
    public static String getTakePhotoFile(String path) {
        FileInputStream is = null;
        try {
            is = new FileInputStream(path);
            File file = new File(path); // 图片文件路径
            if (file.exists()) {
                LogX.d("### 拍照返回图片路径：" + path);
                return path;
            } else {
                ToastUtils.showMsg("该文件不存在!");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public static void selectImageAndTakePhoto(Activity activity, int maxNum) {
        selectImages(activity, maxNum, MimeType.ofImage(), true);
    }

    /**
     * 单张选择
     *
     * @param activity
     */
    public static void selectSingleImage(Activity activity) {
        selectImages(activity, 1, MimeType.ofImage(), true);
    }

    /**
     * 选择多张图片
     *
     * @param activity
     * @param maxNum
     */
    public static void selectImages(Activity activity, int maxNum) {
        selectImages(activity, maxNum, MimeType.ofImage(), false);
    }

    public static void takePhoto(Activity activity, TakePhotoCallback takePhotoCallback) {
        MediaStoreCompat mediaStoreCompat = new MediaStoreCompat(activity);
//        mediaStoreCompat.setCaptureStrategy(new CaptureStrategy(true,"PhotoPicker"));
        mediaStoreCompat.setCaptureStrategy(new CaptureStrategy(true, "com.xmqian.application"));
        mediaStoreCompat.dispatchCaptureIntent(activity, REQUEST_CODE_CHOOSE_PHOTO_SHOOT);
        //因为是指定Uri所以onActivityResult中的data为空 只能再这里获取拍照的路径
        String currentPhotoPath = mediaStoreCompat.getCurrentPhotoPath();
        LogX.d("#### 当前拍摄的照片路径为：" + currentPhotoPath);
        //返回图片路径名
        takePhotoCallback.takePhotoPath(currentPhotoPath);
    }

    public interface TakePhotoCallback {
        //返回拍照存储得路径
        void takePhotoPath(String path);
    }
}
