package com.xmqian.app.common.testdata;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import com.coszero.utilslibrary.utils.LogX;
import com.xmqian.app.common.config.Global;

import java.io.File;
import java.lang.ref.WeakReference;

/*测试工具类*/
public class TestUtils {
    public static final int REQUEST_TAKE_PHOTO_CODE = 111;
    Activity myActivity;
    private static WeakReference weakReference;

    public TestUtils(Activity activity) {
        weakReference = new WeakReference<>(activity);
        myActivity = (Activity) weakReference.get();
    }

    public static TestUtils newInstance(Activity activity) {
        TestUtils testUtils = new TestUtils(activity);
        return testUtils;
    }

    /**
     * 拍照
     */
    public void takePhoto() {
        Uri mUri;
        // 步骤一：创建存储照片的文件
        String path = Global.CameraPath;
        File file = new File(path, "test.jpg");
        if (!file.getParentFile().exists())
            file.getParentFile().mkdirs();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            //步骤二：Android 7.0及以上获取文件 Uri
            // getUriForFile的第二个参数需要与清单文件中配置的authorities属性的值相同
            mUri = FileProvider.getUriForFile(myActivity, "com.xmqian.application", file);
            LogX.i("通过FileProvider获取图片路径");
        } else {
            //步骤三：获取文件Uri
            mUri = Uri.fromFile(file);
            LogX.i("正常方式获取图片路径");
        }
        //步骤四：调取系统拍照
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mUri);
        myActivity.startActivityForResult(intent, REQUEST_TAKE_PHOTO_CODE);
    }
}
