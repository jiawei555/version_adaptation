package com.xmqian.app.common.config.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by xuyougen on 2018/4/11.
 */

public class RetrofitHelper {

    private static final int CONNECT_TIMEOUT = 8;
    private static final int READ_TIMEOUT = 8;
    private static final int WRITE_TIMEOUT = 8;

    private Retrofit mRetrofit;
    private static RetrofitHelper INSTANCE;

    private RetrofitHelper() {
        init();
    }

    private void init() {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .addInterceptor(new HttpInterceptorUtils())
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS).build();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(ApiFactory.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
    }

    public static RetrofitHelper getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RetrofitHelper();
        }
        return INSTANCE;
    }

    public ApiService getService() {
        return mRetrofit.create(ApiService.class);
    }

}
