package com.xmqian.app.project.ui.user.contract;

import com.xmqian.app.common.base.mvp.BaseView;

public interface LoginContract {
    interface Model {
    }

    interface View extends BaseView {
        void onLoginSucceed(String message);

        void onRequestFailed(String message);

        void getCodeSuccess();
    }

    interface Presenter {
        /**
         * 登录
         *
         * @param username
         * @param pwd
         */
        void doLogin(String username, String pwd);

        /**
         * 快捷登录
         *
         * @param username
         * @param smsCode
         */
        void quickLogin(String username, String smsCode);

        /**
         * 获取验证码
         *
         * @param type
         * @param phone
         */
        void getSmsCode2(int type, String phone);
    }
}
