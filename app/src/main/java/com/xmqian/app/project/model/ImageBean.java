package com.xmqian.app.project.model;

/**
 * Desc： 图片或者文件的实体类
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/9/19 18:29
 */
public class ImageBean {
    private String path;//图片路径
    private String filepath;//文件路径

    public String getPath() {
        return path;
    }

    public String getFilepath() {
        return filepath;
    }
}
