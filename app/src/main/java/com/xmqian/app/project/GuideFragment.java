package com.xmqian.app.project;

import android.os.Bundle;
import android.widget.ImageView;

import com.coszero.utilslibrary.utils.LogX;
import com.coszero.utilslibrary.utils.SharedPreUtils;
import com.xmqian.app.R;
import com.xmqian.app.common.base.MyPagerLazyLoadFragment;
import com.xmqian.app.common.config.Constant;
import com.xmqian.app.project.ui.user.activity.LoginActivity;

import butterknife.BindView;

/**
 * Desc： 引导界面
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/6/10 16:16
 *
 * @link(com.xmqian.app.project.GuideActivity)
 */
public class GuideFragment extends MyPagerLazyLoadFragment {
    @BindView(R.id.iv_guide)
    ImageView mIvGuide;

    private int[] resoursArray;
    public static final String RESOURS_ARRAY = "resoursArray";
    public static final String POS = "pos";
    private int index;
    private boolean isFrist = true;

    public static GuideFragment newInstance(int[] resoursArray, int pos) {
        Bundle args = new Bundle();
        GuideFragment fragment = new GuideFragment();
        args.putIntArray(RESOURS_ARRAY, resoursArray);
        args.putInt(POS, pos);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void initView() {
        resoursArray = getArguments().getIntArray(RESOURS_ARRAY);
        index = getArguments().getInt(POS, 0);
        mIvGuide.setImageResource(resoursArray[index]);
        if (index == 0) {
            mIvGuide.setImageResource(resoursArray[index]);
        }
        if (resoursArray.length - 1 == index) {
            mIvGuide.setOnClickListener(v -> {
                SharedPreUtils.saveInteger(context, Constant.JUMP_TYPE, Constant.GUIDE_FINISH);
                LoginActivity.startNewTask(getActivity());
                getActivity().finish();
            });
        }
        isFrist = false;
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_guide;
    }

    @Override
    public void lazyLoadData() {
        if (resoursArray != null && !isFrist) {
            mIvGuide.setBackgroundResource(resoursArray[index]);
            LogX.i("### 引导加载第" + (index + 1) + "页");
        }
    }
}
