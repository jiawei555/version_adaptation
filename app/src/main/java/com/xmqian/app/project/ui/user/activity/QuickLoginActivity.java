package com.xmqian.app.project.ui.user.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import com.coszero.uilibrary.widget.CountDownTextView;
import com.coszero.utilslibrary.utils.ToastUtils;
import com.xmqian.app.R;
import com.xmqian.app.common.base.mvp.BaseMvpActivity;
import com.xmqian.app.project.MainActivity;
import com.xmqian.app.project.event.LoginEvent;
import com.xmqian.app.project.ui.user.contract.LoginContract;
import com.xmqian.app.project.ui.user.presenter.LoginPresenter;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

public class QuickLoginActivity extends BaseMvpActivity<LoginContract.View, LoginPresenter> implements LoginContract.View {


    @BindView(R.id.tv_get_code)
    CountDownTextView mTvGetCode;
    @BindView(R.id.btn_login)
    Button mBtnLogin;
    @BindView(R.id.et_user)
    EditText mEtUser;
    @BindView(R.id.et_sms_code)
    EditText mEtPwd;
    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            setLoginBtnState();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setShowBack(false);
        setTitle("快捷登录");
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initView() {
        mEtPwd.addTextChangedListener(textWatcher);
        mEtUser.addTextChangedListener(textWatcher);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected LoginPresenter createPresenter() {
        return new LoginPresenter();
    }

    /**
     * 注册
     *
     * @param view
     */
    public void register(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    private void getSmsCode() {
        if (mEtUser.length() == 11) {
            mPresenter.getSmsCode2(3, mEtUser.getText().toString());
        } else {
            ToastUtils.showMsg("手机号长度不正确");
        }
    }

    @OnClick({R.id.btn_login, R.id.tv_get_code})
    void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login://登录
                String userName = mEtUser.getText().toString();
                String passWord = mEtPwd.getText().toString();
                mPresenter.quickLogin(userName, passWord);
                break;
            case R.id.tv_get_code://获取验证码
                getSmsCode();
                break;
            case R.id.tv_pass_login://跳转密码登录
                LoginActivity.startTask(this);
                break;
        }
    }

    @Override
    public void showLoadingView() {
        showLoadingDialog();
    }

    @Override
    public void hideLoadingView() {
        hideLoadingDialog();
    }

    @Override
    public void onNetworkError() {
        hideLoadingDialog();
    }

    public void setLoginBtnState() {
        if (checkUserInput()) {
            mBtnLogin.setEnabled(true);
        } else {
            mBtnLogin.setEnabled(false);
        }
    }

    public boolean checkUserInput() {
        return mEtUser.length() == 11 && mEtPwd.length() >= 4;
    }

    @Override
    public void onLoginSucceed(String message) {
        ToastUtils.showMsg(message);
        startActivity(new Intent(this, MainActivity.class));
        EventBus.getDefault().post(new LoginEvent(true));
    }

    @Override
    public void onRequestFailed(String message) {

    }

    @Override
    public void getCodeSuccess() {
        //验证码发送成功
    }


    @Override
    protected int getContentLayoutRes() {
        return R.layout.activity_login_quick;
    }
}
