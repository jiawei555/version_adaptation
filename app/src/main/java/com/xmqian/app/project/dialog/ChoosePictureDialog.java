package com.xmqian.app.project.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.lxj.xpopup.core.BottomPopupView;
import com.xmqian.app.R;

/**
 * Desc： 选择图片弹窗
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/8/2 17:50
 */
public class ChoosePictureDialog extends BottomPopupView {
    ChoosePictureCallBack pictureCallBack;
    private String tipText = "更换头像";//头部提示文字

    public ChoosePictureDialog(@NonNull Context context, ChoosePictureCallBack pictureCallBack) {
        super(context);
        this.pictureCallBack = pictureCallBack;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.dialog_choose_picture;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        TextView mTvCancel = findViewById(R.id.tv_cancel);
        TextView mTvGallery = findViewById(R.id.tv_gallery);
        TextView mTvCapture = findViewById(R.id.tv_capture);
        TextView mTvTipText = findViewById(R.id.tv_type_text);
        mTvTipText.setText(tipText);
        mTvCancel.setOnClickListener(v -> dismiss());
        mTvGallery.setOnClickListener(v -> {
            dismiss();
            pictureCallBack.selectGallery();
        });
        mTvCapture.setOnClickListener(v -> {
            dismiss();
            pictureCallBack.selectCapture();
        });
    }

    public void setTipText(String tipText) {
        this.tipText = tipText;
    }

    public interface ChoosePictureCallBack {
        void selectGallery();

        void selectCapture();
    }
}
