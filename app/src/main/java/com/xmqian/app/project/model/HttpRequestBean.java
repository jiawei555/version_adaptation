package com.xmqian.app.project.model;

import com.coszero.utilslibrary.app.AppCheckUtils;

import java.io.Serializable;

/**
 * Desc： 实体类的父类，继承Serializable接口是为了混淆方便
 *
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/7/3 20:49
 */
public class HttpRequestBean<T> implements Serializable {
    private int result;//返回码
    private String msg;//请求结果信息
    private T data;

    public int getResult() {
        return result;
    }

    public String getMsg() {
        return AppCheckUtils.checkString(msg);
    }

    public T getData() {
        return data;
    }
}
