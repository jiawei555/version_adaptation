package com.xmqian.app.project;

import android.app.Application;
import android.content.Context;

import com.coszero.utilslibrary.utils.ToastUtils;
import com.xmqian.app.R;
import com.xmqian.app.common.config.Global;
import com.xmqian.app.common.utils.logger.LoggerUtil;
import com.xmqian.app.common.utils.umeng.UMengAnalyticsUtils;


/**
 * @author xmqian
 * @date 2018/12/28 14:29
 * @desc 自定义Application
 */
public class MyApplication extends Application {
    /**
     * 系统上下文
     */
    private static Context mAppContext;

    /**
     * 获取系统上下文
     */
    public static Context getAppContext() {
        return mAppContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mAppContext = getApplicationContext();
        ToastUtils.init((Application) mAppContext);
        LoggerUtil.initLogger(getResources().getString(R.string.app_name));
        UMengAnalyticsUtils.initUmeng(this, Global.UM_KEY);
    }

}
