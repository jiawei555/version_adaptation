package com.xmqian.app.project.event;

/**
 * Desc： 登录状态广播类,接收退出登录与已登录两种状态
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/6/10 11:19
 */
public class LoginEvent {
    private boolean isLogin;

    public LoginEvent(boolean isLogin) {
        this.isLogin = isLogin;
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean login) {
        isLogin = login;
    }
}
