package com.xmqian.app.project.ui.common.presenter;

import com.xmqian.app.common.base.mvp.BasePresenter;
import com.xmqian.app.project.ui.common.contract.MyWebViewContract;

public class MyWebViewPresenter extends BasePresenter<MyWebViewContract.View> implements MyWebViewContract.Presenter {
    /**
     * 获取网页地址
     *
     * @param pageId
     */
    public void getSinglePage(int pageId) {

    }

    public void getFeedBackInfo(int pageId) {

    }
}
