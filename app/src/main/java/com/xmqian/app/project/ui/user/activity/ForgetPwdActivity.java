package com.xmqian.app.project.ui.user.activity;

import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import com.coszero.uilibrary.widget.CountDownTextView;
import com.xmqian.app.R;
import com.xmqian.app.common.base.mvp.BaseMvpActivity;
import com.xmqian.app.project.ui.user.contract.ForgetPwdContract;
import com.xmqian.app.project.ui.user.presenter.ForgetPwdPresenter;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author xmqian
 * @date 2019/5/18 12:17
 * @desc 忘记密码
 */
public class ForgetPwdActivity extends BaseMvpActivity<ForgetPwdContract.View, ForgetPwdPresenter> implements ForgetPwdContract.View, TextWatcher {
    @BindView(R.id.et_user)
    EditText etUser;
    @BindView(R.id.et_sms_code)
    EditText etSmsCode;
    @BindView(R.id.tv_get_code)
    CountDownTextView tvGetCode;
    @BindView(R.id.btn_next)
    Button btnNext;

    private String mSmsCode;
    private String mPhone;

    @Override
    protected int getContentLayoutRes() {
        return R.layout.activity_forget_pwd;
    }

    @Override
    protected void initView() {
        setTitle(getResources().getString(R.string.title_forget_pwd));
        etUser.addTextChangedListener(this);
        etSmsCode.addTextChangedListener(this);
    }

    @Override
    protected void initData() {
        requestTranslucentStatusBar(Color.parseColor("#FFFFFF"), true);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (etSmsCode.length() >= 4 && etUser.length() == 11) {
            btnNext.setEnabled(true);
        } else {
            btnNext.setEnabled(false);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    protected ForgetPwdPresenter createPresenter() {
        return new ForgetPwdPresenter();
    }

    @Override
    public void showLoadingView() {
        showLoadingDialog();
    }

    @Override
    public void hideLoadingView() {
        hideLoadingDialog();
    }

    @Override
    public void onNetworkError() {
        hideLoadingDialog();
    }


    @OnClick(R.id.tv_get_code)
    public void onTvGetCodeClicked() {
       /* if (etUser.length() == 11) {
            new PicCodeDialog.Builder(this)
                    .setListener(new PicCodeDialog.OnSendSmsSuccessListener() {
                        @Override
                        public void onSuccess(Dialog dialog) {
                            dialog.dismiss();
                            tvGetCode.startCountDown(59);
                        }
                    })
                    .setType(PicCodeDialog.Builder.TYPE_RESET)
                    .setPhoneNum(etUser.getText().toString())
                    .build().show();
        } else {
            ToastHelper.getInstance().showWarn("请填写正确的手机号码");
        }*/
    }

    @OnClick(R.id.btn_next)
    public void onBtnNextClicked() {
        mSmsCode = etSmsCode.getText().toString();
        mPhone = etUser.getText().toString();
        mPresenter.forgetnew1(mPhone, mSmsCode);
    }

}
