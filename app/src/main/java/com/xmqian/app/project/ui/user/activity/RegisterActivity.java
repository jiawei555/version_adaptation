package com.xmqian.app.project.ui.user.activity;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import com.coszero.uilibrary.widget.CountDownTextView;
import com.coszero.utilslibrary.utils.ToastUtils;
import com.xmqian.app.R;
import com.xmqian.app.common.base.mvp.BaseMvpActivity;
import com.xmqian.app.project.MainActivity;
import com.xmqian.app.project.ui.user.contract.RegisterContract;
import com.xmqian.app.project.ui.user.presenter.RegisterPresenter;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * @author xmqian
 * @date 2019/5/18 12:18
 * @desc 用户注册
 */
public class RegisterActivity extends BaseMvpActivity<RegisterContract.View, RegisterPresenter> implements RegisterContract.View {

    @BindView(R.id.tv_get_code)
    CountDownTextView mTvGetCode;
    @BindView(R.id.btn_reg)
    Button mBtnReg;
    @BindView(R.id.et_phone)
    EditText mEtPhone;
    @BindView(R.id.et_pwd)
    EditText mEtPwd;
    @BindView(R.id.et_sms_code)
    EditText mEtSmsCode;
    @BindView(R.id.sw_agree)
    Switch mSwAgree;
    /*输入字符监听*/
    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            setRegBtnState();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void initView() {
        setTitle("注册");
        mEtPwd.addTextChangedListener(textWatcher);
        mEtPhone.addTextChangedListener(textWatcher);
        mEtSmsCode.addTextChangedListener(textWatcher);

        mSwAgree.setOnCheckedChangeListener((buttonView, isChecked) -> setRegBtnState());
    }

    @Override
    protected void initData() {

    }

    public void getSmsCode() {
        if (mEtPhone.length() == 11) {
           /* new PicCodeDialog.Builder(RegisterActivity.this)
                    .setPhoneNum(mEtPhone.getText().toString())
                    .setListener(dialog -> {
                        dialog.dismiss();
                        mTvGetCode.startCountDown(59);
                    })
                    .setType(PicCodeDialog.Builder.TYPE_REGISTER)
                    .build()
                    .show();*/
        } else {
            ToastUtils.showMsg("手机号长度不正确");
        }
    }

    /**
     * 立即注册
     */
    public void register() {
        mPresenter.register(mEtPhone.getText().toString(), mEtPwd.getText().toString(), mEtSmsCode.getText().toString());
    }

    /**
     * 获取用户协议
     */
    @OnClick({R.id.tv_user_protocol, R.id.tv_jump_login, R.id.tv_get_code, R.id.btn_reg})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_jump_login://注册成功，跳转登录
                LoginActivity.startTask(this);
                break;
            case R.id.tv_user_protocol://用户协议
//        startActivity(MyWebViewFragment.getIntent(this, MyWebViewFragment.TYPE_SINGLE_PAGE, 8));
                break;
            case R.id.tv_get_code://获取验证码
                getSmsCode();
                break;
            case R.id.btn_reg://注册
                register();
                break;
        }
    }


    /**
     * 设置注册按钮的启用状态
     */
    private void setRegBtnState() {
        if (isUserInputDataIsValid()) {
            mBtnReg.setEnabled(true);
        } else {
            mBtnReg.setEnabled(false);
        }
    }

    /**
     * 用户输入的内容是否达到条件要求
     *
     * @return
     */
    public boolean isUserInputDataIsValid() {
        return mEtPwd.length() >= 6 && mEtPhone.length() == 11 && mEtSmsCode.length() >= 4 && mSwAgree.isChecked();
    }

    //点击小眼睛 是否显示密码相关
    @OnCheckedChanged(R.id.cb_pwd_status)
    public void toggleState(boolean isChecked) {
        if (isChecked) {
            //明文密码
            mEtPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            mEtPwd.setSelection(mEtPwd.getText().toString().length());
        } else {
            //显示密码
            mEtPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mEtPwd.setSelection(mEtPwd.getText().toString().length());
        }
    }

    @Override
    protected RegisterPresenter createPresenter() {
        return new RegisterPresenter();
    }

    @Override
    public void onRegisterSucceed(String message) {
        mPresenter.doLogin(mEtPhone.getText().toString(), mEtPwd.getText().toString());
    }

    @Override
    public void onRequestFailed(String message) {
        ToastUtils.showMsg(message);
    }

    @Override
    public void onLoginSucceed(String message) {
        ToastUtils.showMsg(message);
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void showLoadingView() {
        showLoadingDialog();
    }

    @Override
    public void hideLoadingView() {
        hideLoadingDialog();
    }

    @Override
    public void onNetworkError() {

    }

    @Override
    protected int getContentLayoutRes() {
        return R.layout.activity_register;
    }
}
