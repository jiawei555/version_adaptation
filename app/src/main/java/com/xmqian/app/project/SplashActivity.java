package com.xmqian.app.project;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.coszero.utilslibrary.utils.LogX;
import com.coszero.utilslibrary.utils.SharedPreUtils;
import com.coszero.utilslibrary.utils.ToastUtils;
import com.xmqian.app.R;
import com.xmqian.app.common.callback.PermissionRequestCallBack;
import com.xmqian.app.common.config.AppConfig;
import com.xmqian.app.common.config.Constant;
import com.xmqian.app.common.testdata.TestMainActivity;
import com.xmqian.app.common.utils.apiadapter.DisplayCutoutUtils;
import com.xmqian.app.common.utils.apiadapter.PermissionRequestUtils;
import com.xmqian.app.common.utils.glide.ImageLoad;
import com.xmqian.app.project.ui.user.activity.LoginActivity;

/**
 * @author xmqian
 * @date 2018/12/27 17:46
 * @desc 过渡界面，可以获取App状态值，判断是否直接进入应用
 */
public class SplashActivity extends AppCompatActivity {
    private TextView tvCountdown;
    private ImageView ivSplash;
    private int times = 3;//倒计时时间
    private int jumpType = 0;//获取app的状态值，判断跳转哪个界面
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            LogX.i("####过渡界面跳转延迟" + msg.what + "秒");
            if (msg.what != 0) {
                tvCountdown.setText("跳过" + msg.what + "s");
                times--;
                handler.sendEmptyMessageDelayed(times, 1000);
            } else {
                goActivity();
            }
            super.handleMessage(msg);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        DisplayCutoutUtils.newInstance(this).setDisplayCutouMode(DisplayCutoutUtils.SHORT_EDGES);
        jumpType = SharedPreUtils.getIntegerData(this, Constant.JUMP_TYPE, AppConfig.INTO_GUIDE ? 0 : 1);
        checkPermission();
    }

    private void checkPermission() {
        PermissionRequestUtils.requestEasyPermission(this, new PermissionRequestCallBack() {
            @Override
            public void requestSuccess() {
                if (AppConfig.DELAY_SPLASH) {
                    initView();
                } else {
                    goActivity();
                }
            }

            @Override
            public void requestFaile() {
                ToastUtils.showMsg("拒绝授予权限会影响应用部分功能的使用");
            }
        });
    }

    private void initView() {
        ivSplash = findViewById(R.id.iv_splash);
        tvCountdown = findViewById(R.id.tv_countdown);
        tvCountdown.setOnClickListener(v -> {
            handler.removeMessages(times);
            goActivity();
        });
        //过渡界面是否延迟
        showData();
    }

    /**
     * 跳转界面，根据获取的值判断进入哪个界面
     */
    private void goActivity() {
        Intent intent = new Intent();
        switch (jumpType) {
            case 0://第一次安装并启动app,进入引导界面
                intent.setClass(this, GuideActivity.class);
                break;
            case 1://进入过引导界面，并且用户手动点击进入登录界面，进入登录界面
                if (AppConfig.NEED_FRIST_LOGIN) {
                    intent.setClass(this, LoginActivity.class);
                } else {
                    //普通主页
//                    intent.setClass(this, MainActivity.class);
                    //底部菜单栏的主页
//                    intent.setClass(this, MainBottomTabActivity.class);
                    //测试主页
                    intent.setClass(this, TestMainActivity.class);
                }
                break;
            case 2://已经登录，直接进入主界面
                intent.setClass(this, MainActivity.class);
                break;
            default:
                break;
        }
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
        super.onDestroy();
    }

    /**
     * 过渡图片需要动态获取，使用此方法
     */
    public void showData() {
        //执行闪屏
        ImageLoad.loadImage(this, /*Global.SPLASH_IMAGE_URL*/R.drawable.splash_image, ivSplash, new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                LogX.e("####SplashActivity", "过渡界面网络图片加载失败,请检查网络或链接地址是否有误");
                goActivity();
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                //图片加载成功
                tvCountdown.setVisibility(View.VISIBLE);
                tvCountdown.setText("跳过" + times + "s");
                handler.sendEmptyMessage(times);
                return false;
            }
        });
    }
}
