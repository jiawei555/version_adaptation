package com.xmqian.app.project.model;

import com.coszero.utilslibrary.app.AppCheckUtils;
/**
 * Desc： 横幅BannerView实体
 *
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/9/23 10:40
 */
public class BannerBean {

    /**
     * Name : 广告名称
     * Pic : 广告图片
     * Url : 广告跳转地址
     * UrlType : 跳转类型 1是外部链接 2是app内部跳转
     * SystemClass : 系统ID
     * SystemClassVal : 系统分类
     */

    private String Name;
    private String Pic;
    private String Url;
    private String UrlType;
    private String SystemClass;
    private String SystemClassVal;

    public BannerBean(String pic) {
        Pic = pic;
    }

    public BannerBean() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getPic() {
        return Pic;
    }

    public void setPic(String Pic) {
        this.Pic = Pic;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String Url) {
        this.Url = Url;
    }

    public String getUrlType() {
        return AppCheckUtils.checkIntStr(UrlType);
    }

    public void setUrlType(String UrlType) {
        this.UrlType = UrlType;
    }

    public String getSystemClass() {
        return SystemClass;
    }

    public void setSystemClass(String SystemClass) {
        this.SystemClass = SystemClass;
    }

    public String getSystemClassVal() {
        return SystemClassVal;
    }

    public void setSystemClassVal(String SystemClassVal) {
        this.SystemClassVal = SystemClassVal;
    }

    @Override
    public String toString() {
        return getPic();
    }
}
