package com.xmqian.app.project.ui.user.contract;

import com.xmqian.app.common.base.mvp.BaseView;

public interface ForgetPwdContract {
    interface View extends BaseView {

    }

    interface Presenter {
        void forgetnew1(String phone, String smsCode);

        void forgetnew2(String mobile, String pwd, String confpwd);
    }
}
