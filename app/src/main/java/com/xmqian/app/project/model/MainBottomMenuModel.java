package com.xmqian.app.project.model;

/**
 * Desc： 主页底部菜单
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/9/23 14:25
 */
public class MainBottomMenuModel {
    private int topDrawable;//切换图标
    private String tabName;//底部名称
    private int viewId;//控件id

    /**
     * @param topDrawable 顶部切换图标 R.drawable.sl_main_page1
     * @param tabName 菜单名称
     * @param viewId 控件id R.id.tab  可在ids.xml文件中添加动态id
     */
    public MainBottomMenuModel(int topDrawable, String tabName, int viewId) {
        this.topDrawable = topDrawable;
        this.tabName = tabName;
        this.viewId = viewId;
    }

    public int getTopDrawable() {
        return topDrawable;
    }

    public void setTopDrawable(int topDrawable) {
        this.topDrawable = topDrawable;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    public int getViewId() {
        return viewId;
    }

    public void setViewId(int viewId) {
        this.viewId = viewId;
    }
}
