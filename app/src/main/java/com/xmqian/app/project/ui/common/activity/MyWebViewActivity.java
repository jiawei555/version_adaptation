package com.xmqian.app.project.ui.common.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.xmqian.app.R;
import com.xmqian.app.common.base.MyBaseActivity;
import com.xmqian.app.project.ui.common.fragment.MyWebViewFragment;

/**
 * Desc： 网页展示
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/9/20 11:27
 */
public class MyWebViewActivity extends MyBaseActivity implements MyWebViewFragment.TitleCallBack {
    private static final String EXTRA_TYPE = "mCurrentType";
    private static final String EXTRA_PAGE_ID = "pageId";
    private static final String EXTRA_TEXT = "text";
    private static final String EXTRA_TITLE = "title";
    private FragmentManager supportFragmentManager;

    /**
     * 获取intent，启动网页或打开内容
     */
    public static Intent getIntent(Context context, int type, int pageId) {
        Intent intent = new Intent(context, MyWebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, type);
        intent.putExtra(EXTRA_PAGE_ID, pageId);
        return intent;
    }

    /**
     * 获取intent，启动网页或打开内容
     *
     * @param context
     * @param title 要显示的标题
     * @param url url、文本/html 片段
     * @return
     */
    public static Intent getIntent(Context context, String title, String url) {
        Intent intent = new Intent(context, MyWebViewActivity.class);
        intent.putExtra(EXTRA_TITLE, title);
        intent.putExtra(EXTRA_TEXT, url);
        return intent;
    }

    /**
     * 获取intent，打开特定的页面，如：在线支付，用于检测服务器返回的结果（json字符串）
     *
     * @param context
     * @param title 要显示的标题
     * @param url url 地址
     * @param type 本类所定义的常量值，如 TYPE_BANK_PAYMENT
     * @return
     */
    public static Intent getIntent(Context context, String title, String url, int type) {
        return getIntent(context, title, url).putExtra(EXTRA_TYPE, type);
    }

    @Override
    protected void initView() {
        requestTranslucentStatusBar(Color.parseColor("#FFFFFF"), true);
        int pageId = getIntent().getIntExtra(EXTRA_PAGE_ID, 0);
        int type = getIntent().getIntExtra(EXTRA_TYPE, -1);
        String text = getIntent().getStringExtra(EXTRA_TEXT);
        String title = getIntent().getStringExtra(EXTRA_TITLE);
        MyWebViewFragment myWebViewFragment = MyWebViewFragment.newInstance(type, pageId, title, text);
        supportFragmentManager = getSupportFragmentManager();
        supportFragmentManager.beginTransaction().replace(R.id.fl_content, myWebViewFragment).commit();
    }

    @Override
    protected int getContentLayoutRes() {
        return R.layout.layout_title_fragment;
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = supportFragmentManager.findFragmentById(R.id.fl_content);
        if (fragment != null && fragment instanceof MyWebViewFragment) {
            if (((MyWebViewFragment) fragment).onBackPressed()) {
                return;
            }
            super.onBackPressed();
        }
    }

    @Override
    public void setTitleListenner(String title) {
        setTitle(title);
    }
}
