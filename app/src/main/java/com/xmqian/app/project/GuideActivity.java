package com.xmqian.app.project;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.xmqian.app.R;
import com.xmqian.app.common.base.MyBaseActivity;
import com.xmqian.app.common.utils.apiadapter.DisplayCutoutUtils;

import butterknife.BindView;

/**
 * Desc： 引导界面
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/6/10 15:36\
 *
 * @link(com.xmqian.app.project.GuideFragment)
 */
public class GuideActivity extends MyBaseActivity {
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    //引导页图片
    private int[] GUIDE_IMAGE = new int[]{R.drawable.guide1, R.drawable.guide2, R.drawable.guide3};
    private GuideViewPageAdapter mAdapter;
//    private List<Fragment> fragments = new ArrayList<>();

    @Override
    protected void initView() {
        DisplayCutoutUtils.newInstance(this).setDisplayCutouMode(DisplayCutoutUtils.SHORT_EDGES);
        mAdapter = new GuideViewPageAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        initViewPagerItem();
    }

    private void initViewPagerItem() {
        mViewPager.setOffscreenPageLimit(GUIDE_IMAGE.length);
        mAdapter.notifyDataSetChanged();
    }

    private class GuideViewPageAdapter extends FragmentPagerAdapter {

        public GuideViewPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return GuideFragment.newInstance(GUIDE_IMAGE, position);
        }

        @Override
        public int getCount() {
            return GUIDE_IMAGE.length;
        }
    }


    @Override
    protected int getContentLayoutRes() {
        return R.layout.activity_guide;
    }
}

