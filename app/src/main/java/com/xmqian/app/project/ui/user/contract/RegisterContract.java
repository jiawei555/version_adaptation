package com.xmqian.app.project.ui.user.contract;

import com.xmqian.app.common.base.mvp.BaseView;

public interface RegisterContract {
    interface View extends BaseView {
        void onRegisterSucceed(String message);

        void onRequestFailed(String message);

        void onLoginSucceed(String message);
    }

    interface Presenter {
        void register(String mobile, String pwd, String code);

        void doLogin(String username, String pwd);
    }
}
