package com.xmqian.app.project.main.fragment;

import android.os.Bundle;

import com.xmqian.app.R;
import com.xmqian.app.common.base.MyBaseFragment;
import com.xmqian.app.common.config.Constant;
import com.xmqian.app.common.testdata.TestData;
import com.xmqian.app.common.widget.banner.BannerUtils;
import com.xmqian.app.common.widget.banner.BannerView;

import butterknife.BindView;

public class MainPage1Fragment extends MyBaseFragment {
    @BindView(R.id.vp_banner)
    BannerView mBranner;

    public static MainPage1Fragment newInstance(String title) {
        Bundle args = new Bundle();
        args.putString(Constant.EXTRA_TITLE, title);
        MainPage1Fragment fragment = new MainPage1Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_main_page1;
    }

    @Override
    protected void initView() {
        setTitle(getArguments().getString(Constant.EXTRA_TITLE));
        BannerUtils.startBanner(mBranner, TestData.getBanners());
    }
}
