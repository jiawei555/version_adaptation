package com.xmqian.app.project.ui.user.presenter;

import com.xmqian.app.common.base.mvp.BasePresenter;
import com.xmqian.app.project.ui.user.contract.LoginContract;

public class LoginPresenter extends BasePresenter<LoginContract.View> implements LoginContract.Presenter {

    /**
     * 登录
     *
     * @param username
     * @param pwd
     */
    @Override
    public void doLogin(String username, String pwd) {
        getView().showLoadingView();

        //请求成功
        getView().hideLoadingView();
        getView().onLoginSucceed("登录成功");
    }

    /**
     * 快捷登录
     *
     * @param username
     * @param smsCode
     */
    @Override
    public void quickLogin(String username, String smsCode) {
        getView().showLoadingView();

        //请求成功
        getView().hideLoadingView();
        getView().onLoginSucceed("登录成功");
    }

    /**
     * 获取验证码
     *
     * @param type
     * @param phone
     */
    @Override
    public void getSmsCode2(int type, String phone) {
        getView().getCodeSuccess();
    }
}
