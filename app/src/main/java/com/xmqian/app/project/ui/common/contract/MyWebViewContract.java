package com.xmqian.app.project.ui.common.contract;

import com.xmqian.app.common.base.mvp.BaseView;
import com.xmqian.app.project.ui.common.model.MyWebViewModel;

public interface MyWebViewContract {
    interface Model {
    }

    interface View extends BaseView {
        void onGetSinglePageSucceed(MyWebViewModel pageBean);

        void onRequestFailed(String message);
    }

    interface Presenter {
    }
}
